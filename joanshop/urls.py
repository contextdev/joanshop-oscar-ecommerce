from django.conf.urls import include, url
from django.contrib import admin

from oscar.app import application
from .settings import MEDIA_ROOT


urlpatterns = [
    url(r'^i18n/', include('django.conf.urls.i18n')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^media/(?P<path>.*)$', "django.views.static.serve", {'document_root': MEDIA_ROOT}),
    url(r'', include(application.urls)),
]
